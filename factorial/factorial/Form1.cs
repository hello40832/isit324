﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace factorial
{
    public partial class frmFact : Form
    {
        public frmFact()
        {
            InitializeComponent();
        }

        private void btnFact_Click(object sender, EventArgs e)
        {
            int number = 0;
            if(int.TryParse(txtNumber.Text, out number))
            {
                if (number < 0)
                {
                    MessageBox.Show("Cannot calculate factorial of negative number.");
                }
                else if (number <= 1)
                {
                    lblOutput.Text = "1";
                }
                else
                {
                    int result = number;
                    for (int i = 2; i < result; i++)
                    {
                        number *= i;
                    }
                    lblOutput.Text = number + "";
                }
                
            }
            else
            {
                MessageBox.Show("Must enter a number in textbox");
            }
        }
    }
}
