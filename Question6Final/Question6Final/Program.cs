﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Question6Final
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Process p = new Process();
            p.StartInfo.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            p.StartInfo.FileName = @"CalculateRate.exe";
            p.StartInfo.Arguments = "100000 10 5";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
