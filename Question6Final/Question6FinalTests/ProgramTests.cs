﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Question6Final;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Question6Final.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void MainTest()
        {
            Process p = new Process();
            p.StartInfo.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            p.StartInfo.FileName = @"CalculateRate.exe";
            p.StartInfo.Arguments = "100000 10 5";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            double result = Convert.ToDouble(p.StandardOutput.ReadLine());
            Assert.AreEqual(1060.66, result);
        }
    }
}