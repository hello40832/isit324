﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Question 1
            Stopwatch stp = new Stopwatch();
            stp.Start();
            f(10);
            stp.Stop();
            Console.WriteLine("Question 1 time: {0}",stp.ElapsedMilliseconds);
            stp.Reset();
            Console.WriteLine("\n");

            //Question 2
            double dblTotTime = 0;
            Console.WriteLine(f(100));
            for(int i =1; i<60; i++)
            {
                stp.Start();
                f(10);
                stp.Stop();
                dblTotTime += stp.ElapsedTicks;
                Console.WriteLine("Question 2, run number {0}, time: {1}",i,stp.ElapsedTicks);
                stp.Reset();
            }
            Console.WriteLine("Question 2 average time: {0}",dblTotTime/5);

            Console.ReadKey();
        }

        static long f(int n)
        {
            long sum = 0;

            for (int i = 1; i < n; i++)
                if (i % 3 == 0 || i % 5 == 0)
                    sum += i;

            return sum;
        }
        
    }
}
