﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(factTest.Startup))]
namespace factTest
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
