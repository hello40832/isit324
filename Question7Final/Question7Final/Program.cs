﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Question7Final
{
    class Program
    {
        static void Main(string[] args)
        {

            Process p = new Process();
            p.StartInfo.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            p.StartInfo.FileName = @"CalculateRate.exe";
            p.StartInfo.Arguments = "100000 10 5";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;


            Stopwatch stp = new Stopwatch();
            for (int i = 1; i <= 10; i++)
            {
                stp.Start();
                p.Start();
                p.WaitForExit();
                stp.Stop();
                Console.WriteLine("Question 7, run number {0}, time: {1}", i, stp.ElapsedMilliseconds);
                stp.Reset();
            }
            
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
