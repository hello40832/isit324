﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        
        [TestMethod]
        public void TestMethod1()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test1.exe " + @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test1.cs";
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test1.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }
        [TestMethod]
        public void TestMethod2()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test2.exe " + @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test2.cs";
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test2.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }
        [TestMethod]
        public void TestMethod3()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test3.exe " + @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test3.cs";
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test3.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }
        [TestMethod]
        public void TestMethod4()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test4.exe " + @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test4.cs";
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test4.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }
        [TestMethod]
        public void TestMethod5()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test5.exe " + @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test5.cs";
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\test5.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }
        [TestMethod]
        public void TestMethodFail()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\failTest.exe " + @"C:\Users\WeiLi\Documents\school\Bellevue\ISIT324\Homework5\Homework5\failTest.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            string result = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            Assert.AreEqual("Microsoft (R) Visual C# Compiler version 4.6.0081.0\n\nfor Microsoft (R) .NET Framework 4.5\nCopyright (C) Microsoft Corporation. All rights reserved.\n\nc:\\Users\\WeiLi\\Documents\\school\\Bellevue\\ISIT324\\Homework5\\Homework5\\failTest.cs(15,27): error CS0019: Operator \'??\' cannot be applied to operands of type \'int\' and \'int?\'\n", result);
        }
    }
}
