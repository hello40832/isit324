﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Question3Final;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question3Final.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void Question3()
        {
            double amount = Microsoft.VisualBasic.Financial.Pmt(5.0 / 100 / 12, 10 * 12, -100000);
            amount = Math.Round(amount * 100) / 100;
            Assert.AreEqual(1060.66,amount);
        }
    }
}