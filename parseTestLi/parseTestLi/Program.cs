﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parseTestLi
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        public static void parseTest()
        {
            if (int.Parse("0") == 0)
            {
                Console.WriteLine("True");
            }
            else if (int.Parse("-1") == -1)
            {
                Console.WriteLine("True");
            }
            else if (int.Parse("+1") == 1)
            {
                Console.WriteLine("True");
            }
            else if (int.Parse("2147483647") == 2147483647)
            {
                Console.WriteLine("True");
            }
            else if (int.Parse("-2147483648") == -2147483647)
            {
                Console.WriteLine("True");
            }
            else if (int.Parse("01") == 1)
            {
                Console.WriteLine("True");
            }
            try
            {
                int.Parse("2147483648");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            try
            {
                int.Parse("-2147483649");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            try
            {
                int.Parse("aa");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            try
            {
                int.Parse("!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            try
            {
                int.Parse("1!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            try
            {
                int.Parse("1!1");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
